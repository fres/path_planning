#!/usr/bin/env python
"""
Copyright (c) 2017, Lars Niklasson
Copyright (c) 2017, Filip Slottner Seholm
Copyright (c) 2017, Fanny Sandblom
Copyright (c) 2017, Kevin Hoogendijk
Copyright (c) 2017, Nils Andren
Copyright (c) 2017, Alicia Gil Martin
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Chalmers University of Technology nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
from math import sqrt, atan2
import os
from collections import deque
import copy

class Point:
    def __init__(self, x , y):
        self.x = x
        self.y = y


class errorCalc:
    def __init__(self, startList):

        self.queue = deque(startList)
        self.p1= self.queue.popleft()
        self.p2= self.queue.popleft()
        self.line = (self.p1,self.p2)

    def calculateError(self, p0):
        (self.p1,self.p2) = self.line
        while self.isAboveEnd(self.p1,self.p2,p0) and len(self.queue)>0:
            tempP1=self.p2
            tempP2=self.queue.popleft()
            self.line= (tempP1,tempP2)
            (self.p1,self.p2) = self.line

        isLeft = ((self.p2.x - self.p1.x)*(p0.y - self.p1.y) - (self.p2.y - self.p1.y)*(p0.x - self.p1.x)) >0 #decides if the error is to the left of centerline or not
        value = abs((self.p2.x - self.p1.x)*(self.p1.y-p0.y) - (self.p1.x-p0.x)*(self.p2.y-self.p1.y)) / (sqrt((self.p2.x-self.p1.x)*(self.p2.x-self.p1.x) + (self.p2.y-self.p1.y)*(self.p2.y-self.p1.y)))
        if(isLeft):
            return -value
        else:
            return value

    def isAboveEnd (self,begin, end, p0):
        #checks if a point is passed the end point of a line.
        if begin.x - end.x !=0 and begin.y - end.y !=0:
            slope = float(begin.y - end.y) / float(begin.x - end.x)
            prependularSlope = (-1)/slope
            prependularM = end.y - end.x*prependularSlope
            if begin.y < end.y:
                #going up
                return (p0.x*prependularSlope + prependularM - p0.y) < 0
            else:
                #going down
                return (p0.x*prependularSlope + prependularM - p0.y) > 0
        elif begin.x - end.x:
            #going straight in x direction
            if begin.x < end.x:
                #going right
                return p0.x > end.x
            else:
                #going left
                return p0.x < end.x
        else:
            #going straight in y direction
            if begin.y < end.y:
                #going up
                return p0.y > end.y
            else:
                #going down
                return p0.y < end.y

    def getDirection(self):
        dy = self.line[1].y - self.line[0].y
        dx = self.line[1].x - self.line[0].x
        theta = atan2(dy, dx)
        return theta

    def isAtEnd(self):
        if len(self.queue)==0:
            return True
        else:
            return False

    def getMaxDistPoint(self, point):
        p1 = self.line[1]
        p0 = self.line[0]
        d1 = sqrt( (point.x - p1.x)**2 + (point.y - p1.y)**2 )
        d0 = sqrt( (point.x - p0.x)**2 + (point.y - p0.y)**2 )
        return max(d1,d0)

    def getCopy(self):
        q = copy.copy(self.queue)
        q.appendleft(self.line[1])
        q.appendleft(self.line[0])
        return errorCalc(q)

    def is_next_Left(self):
        if len(self.queue)>0:
            isLeft = ((self.p2.x - self.p1.x)*(self.queue[0].y - self.p1.y) - (self.p2.y - self.p1.y)*(self.queue[0].x - self.p1.x)) >0
        else:
            isLeft=True
        return isLeft


    def printEC(self):
        print "EC"
        print
        for p in self.queue:
            print p.x, p.y
